/* eslint-disable no-extend-native */
String.prototype.esPalindromo = function () {
  let fraseString = this.toLowerCase().replace(/ /g, '');
  let fraseArray = fraseString.split('');
  let fraseReves = [...fraseArray].reverse();
  return fraseArray.join('') === fraseReves.join('');
};


let frase = 'A casa cal refer la casaca';
console.log(frase.esPalindromo());
